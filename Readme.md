### Prerequisites
 Java 11 version

### Instructions
1. Clone the repo
   
    `git clone https://gitlab.com/valentine.sukhow/route-between-countries`
   
2. Open project with your IDE

3. Run class
   
   `RouteApplication`

   Your application will start on 8080 port.

   If needs to change it, got to `resources -> application.yml`

4. Use Postman, browser or curl request to get the response

`localhost:8080/api/routing/{ORIGIN_COUNTRY}/{DESTINATION_COUNTRY}`

`curl --location --request GET 'localhost:8080/api/routing/ORIGIN_COUNTRY/DESTINATION_COUNTRY' `
   
Examples

`localhost:8080/api/routing/CZE/ITA` -> Response: `{"route":["CZE","AUT","ITA"]}`

`localhost:8080/api/routing/UKR/ITA`  -> Response: `{"route":["UKR","HUN","AUT","ITA"]}`

`localhost:8080/api/routing/FRA/UKR` -> Response: `{"route":["FRA","DEU","POL","UKR"]}`

