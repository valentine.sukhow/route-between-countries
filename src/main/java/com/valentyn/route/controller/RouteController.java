package com.valentyn.route.controller;

import com.valentyn.route.dto.Route;
import com.valentyn.route.service.CalculationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/routing")
public class RouteController {

    @Autowired
    CalculationServiceImpl calculationService;

    @GetMapping("/{origin}/{destination}")
    public ResponseEntity<Route> createHeist (@PathVariable("origin") String originCountry,
                                              @PathVariable("destination") String destinationCountry) {
        return new ResponseEntity<>(calculationService.calculateRoute(originCountry, destinationCountry), HttpStatus.OK);
    }
}
