package com.valentyn.route.dto;

import java.util.List;

public class Route {

    public List<String> route;

    public Route(List<String> route) {
        this.route = route;
    }
}
