package com.valentyn.route.service;

import com.valentyn.route.dto.CountriesDto;
import com.valentyn.route.exception.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class DownloadService{

    @Autowired
    RestTemplate restTemplate;

    @Value("${dataLink}")
    String dataLink;

    public CountriesDto[] getCountriesFromURL() {
        HttpEntity httpEntity = new HttpEntity(new HttpHeaders());
        ResponseEntity<CountriesDto[]> response = restTemplate.exchange(dataLink, HttpMethod.GET, httpEntity, CountriesDto[].class);

        CountriesDto[] body = response.getBody();
        if (body != null) {
            return body;
        } else {
            throw new ApiException("Unexpected response from URL", HttpStatus.CONFLICT);
        }
    }
}
