package com.valentyn.route.service;

import com.valentyn.route.dto.Route;

public interface CalculationService {

    Route calculateRoute(String origin, String destination);

}
