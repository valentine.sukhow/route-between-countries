package com.valentyn.route.service;

import com.google.common.collect.BiMap;
import com.valentyn.route.dto.Route;
import com.valentyn.route.exception.ApiException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class CalculationServiceImpl implements CalculationService {

    final ArrayList<ArrayList<Integer>> connectedVerticals;

    final BiMap<String, Integer> countriesBiMap;

    public CalculationServiceImpl(StorageService service) {
        connectedVerticals = service.storeCountryData();
        countriesBiMap = service.countriesBiMap;
    }

    @Override
    public Route calculateRoute(String origin, String destination) {
        return getShortRoute(countriesBiMap.get(origin), countriesBiMap.get(destination));
    }

    private Route getShortRoute(int origin, int destination) {
        int length = countriesBiMap.size();
        int[] predecessor = new int[length];
        int[] distance = new int[length];

        if (!BFSImplementation(connectedVerticals, origin, destination, length, predecessor, distance)) {
            throw new ApiException("Given source and destination" +
                    "are not connected", HttpStatus.BAD_REQUEST);
        }

        LinkedList<Integer> storedPath = new LinkedList<>();
        int crawl = destination;
        storedPath.add(crawl);
        while (predecessor[crawl] != -1) {
            storedPath.add(predecessor[crawl]);
            crawl = predecessor[crawl];
        }

        ArrayList<String> routeCollection = IntStream.iterate(storedPath.size() - 1, i -> i >= 0, i -> i - 1)
                .mapToObj(i -> countriesBiMap.inverse().get(storedPath.get(i))).collect(Collectors.toCollection(ArrayList::new));
        return new Route(routeCollection);
    }


    private boolean BFSImplementation(ArrayList<ArrayList<Integer>> adj, int src, int dest, int v, int[] pred, int[] dist) {
        LinkedList<Integer> queue = new LinkedList<>();

        boolean[] visited = new boolean[v];

        for (int i = 0; i < v; i++) {
            visited[i] = false;
            dist[i] = Integer.MAX_VALUE;
            pred[i] = -1;
        }

        visited[src] = true;
        dist[src] = 0;
        queue.add(src);

        while (!queue.isEmpty()) {
            int u = queue.remove();
            for (int i = 0; i < adj.get(u).size(); i++) {
                if (!visited[adj.get(u).get(i)]) {
                    visited[adj.get(u).get(i)] = true;
                    dist[adj.get(u).get(i)] = dist[u] + 1;
                    pred[adj.get(u).get(i)] = u;
                    queue.add(adj.get(u).get(i));

                    if (adj.get(u).get(i) == dest)
                        return true;
                }
            }
        }
        return false;
    }
}
