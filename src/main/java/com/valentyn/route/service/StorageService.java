package com.valentyn.route.service;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.valentyn.route.dto.CountriesDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class StorageService {

    private final DownloadService downloadService;

    CountriesDto[] countries;

    BiMap<String, Integer> countriesBiMap;

    public StorageService(DownloadService downloadService) {
        this.downloadService = downloadService;
        countries = downloadService.getCountriesFromURL();
        countriesBiMap = addDataToCountriesBiMap();
    }

    ArrayList<ArrayList<Integer>> storeCountryData() {
        ArrayList<ArrayList<Integer>> connectedVerticals = IntStream.range(0, countries.length)
                .<ArrayList<Integer>>mapToObj(i -> new ArrayList<>()).collect(Collectors.toCollection(ArrayList::new));

        for (int i = 0; i < countries.length; i++) {
            CountriesDto country = countries[i];
            String[] countryBorders = country.borders;
            for (String border : countryBorders) {
                edgeCreation(connectedVerticals, i, countriesBiMap.get(border));
            }
        }
        return connectedVerticals;
    }

    BiMap<String, Integer> addDataToCountriesBiMap() {
        BiMap<String, Integer> countriesBiMap = HashBiMap.create();
        for (int i = 0; i < countries.length; i++) {
            countriesBiMap.put(countries[i].cca3, i);
        }
        return countriesBiMap;
    }

    private void edgeCreation(ArrayList<ArrayList<Integer>> connectedVerticals, int firstVertical, int secondVertical) {
        connectedVerticals.get(firstVertical).add(secondVertical);
        connectedVerticals.get(secondVertical).add(firstVertical);
    }

}