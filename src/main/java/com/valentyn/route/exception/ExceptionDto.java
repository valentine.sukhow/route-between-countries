package com.valentyn.route.exception;


import org.springframework.http.HttpStatus;


public class ExceptionDto {
    
    public HttpStatus status;
    
    public String message;

    public ExceptionDto(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }
}
