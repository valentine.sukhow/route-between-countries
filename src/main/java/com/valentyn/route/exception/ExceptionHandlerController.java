package com.valentyn.route.exception;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<ExceptionDto> handleBackendServiceException(ApiException e){
        ExceptionDto exceptionDto = new ExceptionDto(e.getStatus(), e.getMessage());
        return new ResponseEntity<>(exceptionDto, e.status);
    }
    
}